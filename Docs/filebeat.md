Filebeat is a lightweight shipper for forwarding and centralizing log data. Installed as an agent on your servers, 
Filebeat monitors the log files or locations that you specify, collects log events, and forwards them to either to 
Elasticsearch or Logstash for indexing.

<img src="../images/kibana.png" alt="Your image title" float: right width="550"/>

Here’s how Filebeat works: When you start Filebeat, it starts one or more inputs that look in the locations you’ve specified for log data. For each log that Filebeat locates, Filebeat starts a harvester. Each harvester reads a single log for new content and sends the new log data to libbeat, 
which aggregates the events and sends the aggregated data to the output that you’ve configured for Filebeat.




<img src="../images/filebeat.png" alt="Your image title" float: right width="550"/>


