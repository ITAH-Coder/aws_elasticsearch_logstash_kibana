Create new mapping schema:


```ruby

# curl -H "Content-Type: application/json"  -XPUT 127.0.0.1:9200/movies -d '
{
"mappings": {
"properties": {
   "year": {
      "type": "date"
       }
    }
}
}'

```


Check:

```ruby
# curl -H "Content-Type: application/json"  -XGET 127.0.0.1:9200/movies?pretty
{
  "movies" : {
    "aliases" : { },
    "mappings" : {
      "properties" : {
        "genre" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "id" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "title" : {
          "type" : "text",
          "fields" : {
            "keyword" : {
              "type" : "keyword",
              "ignore_above" : 256
            }
          }
        },
        "year" : {
          "type" : "date"
        }
      }
    },
    "settings" : {
      "index" : {
        "creation_date" : "1566555787659",
        "number_of_shards" : "1",
        "number_of_replicas" : "1",
        "uuid" : "HJxCHMVGRAiWF1SypP6BpQ",
        "version" : {
          "created" : "7030099"
        },
        "provided_name" : "movies"
      }
    }
  }
}
```

Import data form json file:

```ruby

# curl -H "Content-Type: application/json" -XPUT 127.0.0.1:9200/_bulk?pretty --data-binary @movies.json


```

