- Every document has a **_version** filed
- Elsticsearch documents are immutable
- When you update an existing document:
     - a new is created with incremented version
     - th old one is marked for deletion
     
     


Example:
```ruby
[root@els1 ~]# curl -H "Content-Type: application/json" -XPOST 127.0.0.1:9200/movies/_doc/109487/_update -d'
{  "doc": {
           "title": "Interstegggggllar"
       }
}'


```

```ruby
{"_index":"movies","_type":"_doc","_id":"109487","_version":2,"result":"updated","_shards":{"total":2,"successful":1,"failed":0},"_seq_no":5,"_primary_term":1}[root@els1 ~]# curl -H "Content-Type: application/json" -XPOST 127.0.0.1:9200/movies/_doc/109487/_update -d'                             GET 127.0.0.1:9200/movies/_doc/109487?pretty
{
  "_index" : "movies",
  "_type" : "_doc",
  "_id" : "109487",
  "_version" : 2,
  "_seq_no" : 5,
  "_primary_term" : 1,
  "found" : true,
  "_source" : {
    "genre" : [
      "IMAX",
      "Sci-Fi"
    ],
    "title" : "Interstegggggllar",
    "year" : 2014
  }
}
```


Have a look on field **"_version ": 2**
