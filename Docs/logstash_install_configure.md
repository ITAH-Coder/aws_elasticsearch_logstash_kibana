**Installation:**


1. instal java-jdk

```ruby
 yum install java-1.8.0-openjdk
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.hitme.net.pl
 * extras: centos.hitme.net.pl
 * updates: centos2.hti.pl
Resolving Dependencies
--> Running transaction check
---> Package java-1.8.0-openjdk.x86_64 1:1.8.0.222.b10-0.el7_6 will be installed
--> Processing Dependency: java-1.8.0-openjdk-headless(x86-64) = 1:1.8.0.222.b10-0.el7_6 for package: 1:java-1.8.0-openjdk-1.8.0.222.b10-0.el7_6.x86_64
--> Processing Dependency: xorg-x11-fonts-Type1 for package: 1:java-1.8.0-openjdk-1.8.0.222.b10-0.el7_6.x86_64
--> Processing Dependency: libjvm.so(SUNWprivate_1.1)(64bit) for package: 1:java-1.8.0-openjdk-1.8.0.222.b10-0.el7_6.x86_64
--> Processing Dependency: libjpeg.so.62(LIBJPEG_6.2)(64bit) for package: 1:java-1.8.0-openjdk-1.8.0.222.b10-0.el7_6.x86_64
--> Processing Dependency: libjava.so(SUNWprivate_1.1)(64bit) for package: 1:java-1.8.0-openjdk-1.8.0.222.b10-0.el7_6.x86_64
--> Processing Dependency: libasound.so.2(ALSA_0.9.0rc4)(64bit) for package: 1:java-1.8.0-openjdk-1.8.0.222.b10-0.el7_6.x86_64
--> Processing Dependency: libasound.so.2(ALSA_0.9)(64bit) for package: 1:java-1.8.0-openjdk-1.8.0.222.b10-0.el7_6.x86_64
--> Processing Dependency: libXcomposite(x86-64) for package: 1:java-1.8.0-openjdk-1.8.0.222.b10-0.el7_6.x86_64
--> Processing Dependency: gtk2(x86-64) for package: 1:java-1.8.0-openjdk-1.8.0.222.b10-0.el7_6.x86_64
--> Processing Dependency: fontconfig(x86-64) for package: 1:java-1.8.0-openjdk-1.8.0.222.b10-0.el7_6.x86_64
--> Processing Dependency: libjvm.so()(64bit) for package: 1:java-1.8.0-openjdk-1.8.0.222.b10-0.el7_6.x86_64
[..]
```

2. install logstash


```ruby
# yum install logstash
```

3. Configure (load data from /root/access_log example)

```ruby

# cat access_log | more
216.244.66.246 - - [30/Apr/2017:04:28:11 +0000] "GET /docs/triton/pages.html HTTP/1.1" 200 5639 "-" "Mozilla/5.
0 (compatible; DotBot/1.1; http://www.opensiteexplorer.org/dotbot, help@moz.com)"
199.21.99.207 - - [30/Apr/2017:04:29:44 +0000] "GET /docs/triton/class_triton_1_1_wind_fetch-members.html HTTP/
1.1" 200 1845 "-" "Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)"
199.21.99.207 - - [30/Apr/2017:04:29:57 +0000] "GET /docs/triton/class_triton_1_1_breaking_waves_parameters-mem
bers.html HTTP/1.1" 200 1967 "-" "Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)"
100.43.90.9 - - [30/Apr/2017:04:30:13 +0000] "GET /docs/html/functions_rela.html HTTP/1.1" 200 882 "-" "Mozilla
/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)"
217.182.132.36 - - [30/Apr/2017:04:30:31 +0000] "GET /2012/08/sundog-software-featured-in-august-2012-issue-of-
develop/ HTTP/1.1" 200 14326 "-" "Mozilla/5.0 (compatible; AhrefsBot/5.2; +http://ahrefs.com/robot/)"
54.210.20.202 - - [30/Apr/2017:04:30:58 +0000] "POST /wp-cron.php?doing_wp_cron=1493526658.60173296928405761718
75 HTTP/1.1" 200 - "http://sundog-soft.com/wp-cron.php?doing_wp_cron=1493526658.6017329692840576171875" "WordPr
ess/4.7.4; http://sundog-soft.com"
77.247.181.163 - - [30/Apr/2017:04:30:57 +0000] "GET / HTTP/1.1" 301 - "-" "Mozilla/5.0 (Windows NT 5.1; rv:7.0
.1) Gecko/20100101 Firefox/7.0.1"
46.101.139.248 - - [30/Apr/2017:04:31:00 +0000] "GET / HTTP/1.1" 200 20503 "-" "Mozilla/5.0 (Windows NT 5.1; rv
:7.0.1) Gecko/20100101 Firefox/7.0.1"
54.210.20.202 - - [30/Apr/2017:04:31:03 +0000] "POST /wp-cron.php?doing_wp_cron=1493526663.80204606056213378906
25 HTTP/1.1" 200 - "http://sundog-soft.com/wp-cron.php?doing_wp_cron=1493526663.8020460605621337890625" "WordPr
ess/4.7.4; http://sundog-soft.com"


```

Configure conf file:


```ruby

# cat /etc/logstash/conf.d/logstash.conf
input { 
	file {
		path => "/root/access_log"
		start_position => "beginning"
        }
}

filter {
  grok {
    match => { "message" => "%{COMBINEDAPACHELOG}" }
  }
  date {
    match => [ "timestamp" , "dd/MMM/yyyy:HH:mm:ss Z" ]
  }
}

output {
  elasticsearch { hosts => ["localhost:9200"] }
  stdout { codec => rubydebug }
}

```

5. start logstash:


```ruby
/usr/share/logstash/bin/logstash -f /etc/logstash/conf.d/logstash.conf 


```


Example output:


```ruby


OpenJDK 64-Bit Server VM warning: If the number of processors is expected to increase from one, then you should configure the number of parallel GC threads appropriately using -XX:ParallelGCThreads=N
Thread.exclusive is deprecated, use Thread::Mutex
WARNING: Could not find logstash.yml which is typically located in $LS_HOME/config or /etc/logstash. You can specify the path using --path.settings. Continuing using the defaults
Could not find log4j2 configuration at path /usr/share/logstash/config/log4j2.properties. Using default config which logs errors to the console
[WARN ] 2019-08-26 10:22:42.922 [LogStash::Runner] multilocal - Ignoring the 'pipelines.yml' file because modules or command line options are specified
[INFO ] 2019-08-26 10:22:42.968 [LogStash::Runner] runner - Starting Logstash {"logstash.version"=>"7.3.1"}
[INFO ] 2019-08-26 10:22:45.346 [Converge PipelineAction::Create<main>] Reflections - Reflections took 49 ms to scan 1 urls, producing 19 keys and 39 values 
[INFO ] 2019-08-26 10:22:48.346 [[main]-pipeline-manager] elasticsearch - Elasticsearch pool URLs updated {:changes=>{:removed=>[], :added=>[http://localhost:9200/]}}
[WARN ] 2019-08-26 10:22:49.433 [[main]-pipeline-manager] elasticsearch - Restored connection to ES instance {:url=>"http://localhost:9200/"}
[INFO ] 2019-08-26 10:22:49.854 [[main]-pipeline-manager] elasticsearch - ES Output version determined {:es_version=>7}
[WARN ] 2019-08-26 10:22:49.856 [[main]-pipeline-manager] elasticsearch - Detected a 6.x and above cluster: the `type` event field won't be used to determine the document _type {:es_version=>7}
[INFO ] 2019-08-26 10:22:49.909 [[main]-pipeline-manager] elasticsearch - New Elasticsearch output {:class=>"LogStash::Outputs::ElasticSearch", :hosts=>["//localhost:9200"]}
[INFO ] 2019-08-26 10:22:50.072 [Ruby-0-Thread-5: :1] elasticsearch - Using default mapping template
[INFO ] 2019-08-26 10:22:50.332 [Ruby-0-Thread-5: :1] elasticsearch - Attempting to install template {:manage_template=>{"index_patterns"=>"logstash-*", "version"=>60001, "settings"=>{"index.refresh_interval"=>"5s", "number_of_shards"=>1, "index.lifecycle.name"=>"logstash-policy", "index.lifecycle.rollover_alias"=>"logstash"}, "mappings"=>{"dynamic_templates"=>[{"message_field"=>{"path_match"=>"message", "match_mapping_type"=>"string", "mapping"=>{"type"=>"text", "norms"=>false}}}, {"string_fields"=>{"match"=>"*", "match_mapping_type"=>"string", "mapping"=>{"type"=>"text", "norms"=>false, "fields"=>{"keyword"=>{"type"=>"keyword", "ignore_above"=>256}}}}}], "properties"=>{"@timestamp"=>{"type"=>"date"}, "@version"=>{"type"=>"keyword"}, "geoip"=>{"dynamic"=>true, "properties"=>{"ip"=>{"type"=>"ip"}, "location"=>{"type"=>"geo_point"}, "latitude"=>{"type"=>"half_float"}, "longitude"=>{"type"=>"half_float"}}}}}}}
[INFO ] 2019-08-26 10:22:50.461 [Ruby-0-Thread-5: :1] elasticsearch - Installing elasticsearch template to _template/logstash
[WARN ] 2019-08-26 10:22:50.528 [[main]-pipeline-manager] LazyDelegatingGauge - A gauge metric of an unknown type (org.jruby.specialized.RubyArrayOneObject) has been create for key: cluster_uuids. This may result in invalid serialization.  It is recommended to log an issue to the responsible developer/development team.
[INFO ] 2019-08-26 10:22:50.547 [[main]-pipeline-manager] javapipeline - Starting pipeline {:pipeline_id=>"main", "pipeline.workers"=>1, "pipeline.batch.size"=>125, "pipeline.batch.delay"=>50, "pipeline.max_inflight"=>125, :thread=>"#<Thread:0x65e3646c run>"}
[INFO ] 2019-08-26 10:22:50.878 [Ruby-0-Thread-5: :1] elasticsearch - Creating rollover alias <logstash-{now/d}-000001>
[INFO ] 2019-08-26 10:22:51.687 [Ruby-0-Thread-5: :1] elasticsearch - Installing ILM policy {"policy"=>{"phases"=>{"hot"=>{"actions"=>{"rollover"=>{"max_size"=>"50gb", "max_age"=>"30d"}}}}}} to _ilm/policy/logstash-policy
[INFO ] 2019-08-26 10:22:52.343 [[main]-pipeline-manager] file - No sincedb_path set, generating one based on the "path" setting {:sincedb_path=>"/usr/share/logstash/data/plugins/inputs/file/.sincedb_a5218c9e7d4daa3507d7f9ca836e2953", :path=>["/root/access_log"]}
[INFO ] 2019-08-26 10:22:52.429 [[main]-pipeline-manager] javapipeline - Pipeline started {"pipeline.id"=>"main"}
[INFO ] 2019-08-26 10:22:52.540 [Ruby-0-Thread-1: /usr/share/logstash/lib/bootstrap/environment.rb:6] agent - Pipelines running {:count=>1, :running_pipelines=>[:main], :non_running_pipelines=>[]}
[INFO ] 2019-08-26 10:22:52.634 [[main]<file] observingtail - START, creating Discoverer, Watch with file and sincedb collections
[INFO ] 2019-08-26 10:22:53.962 [Api Webserver] agent - Successfully started Logstash API endpoint {:port=>9600}
/usr/share/logstash/vendor/bundle/jruby/2.5.0/gems/awesome_print-1.7.0/lib/awesome_print/formatters/base_formatter.rb:31: warning: constant ::Fixnum is deprecated
{
       "@version" => "1",
    "httpversion" => "1.1",
       "referrer" => "\"-\"",
           "host" => "els1",
     "@timestamp" => 2017-04-30T04:28:11.000Z,
       "clientip" => "216.244.66.246",
           "auth" => "-",
       "response" => "200",
        "message" => "216.244.66.246 - - [30/Apr/2017:04:28:11 +0000] \"GET /docs/triton/pages.html HTTP/1.1\" 200 5639 \"-\" \"Mozilla/5.0 (compatible; DotBot/1.1; http://www.opensiteexplorer.org/dotbot, help@moz.com)\"",
           "verb" => "GET",
          "agent" => "\"Mozilla/5.0 (compatible; DotBot/1.1; http://www.opensiteexplorer.org/dotbot, help@moz.com)\"",
          "bytes" => "5639",
        "request" => "/docs/triton/pages.html",
      "timestamp" => "30/Apr/2017:04:28:11 +0000",
           "path" => "/root/access_log",
          "ident" => "-"
}
{
       "@version" => "1",
    "httpversion" => "1.1",
       "referrer" => "\"-\"",
           "host" => "els1",
     "@timestamp" => 2017-04-30T04:29:44.000Z,
       "clientip" => "199.21.99.207",
           "auth" => "-",
       "response" => "200",
        "message" => "199.21.99.207 - - [30/Apr/2017:04:29:44 +0000] \"GET /docs/triton/class_triton_1_1_wind_fetch-members.html HTTP/1.1\" 200 1845 \"-\" \"Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)\"",
           "verb" => "GET",
          "agent" => "\"Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)\"",
          "bytes" => "1845",
        "request" => "/docs/triton/class_triton_1_1_wind_fetch-members.html",
      "timestamp" => "30/Apr/2017:04:29:44 +0000",
           "path" => "/root/access_log",
          "ident" => "-"
}
{
       "@version" => "1",
    "httpversion" => "1.1",
       "referrer" => "\"-\"",
           "host" => "els1",
     "@timestamp" => 2017-04-30T04:29:57.000Z,
       "clientip" => "199.21.99.207",
           "auth" => "-",
       "response" => "200",
        "message" => "199.21.99.207 - - [30/Apr/2017:04:29:57 +0000] \"GET /docs/triton/class_triton_1_1_breaking_waves_parameters-members.html HTTP/1.1\" 200 1967 \"-\" \"Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)\"",
           "verb" => "GET",
          "agent" => "\"Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)\"",
          "bytes" => "1967",
        "request" => "/docs/triton/class_triton_1_1_breaking_waves_parameters-members.html",
      "timestamp" => "30/Apr/2017:04:29:57 +0000",
           "path" => "/root/access_log",
          "ident" => "-"
}
{
       "@version" => "1",
    "httpversion" => "1.1",
       "referrer" => "\"-\"",
           "host" => "els1",
     "@timestamp" => 2017-04-30T04:30:13.000Z,
       "clientip" => "100.43.90.9",
           "auth" => "-",
       "response" => "200",
        "message" => "100.43.90.9 - - [30/Apr/2017:04:30:13 +0000] \"GET /docs/html/functions_rela.html HTTP/1.1\" 200 882 \"-\" \"Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)\"",
           "verb" => "GET",
          "agent" => "\"Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)\"",
          "bytes" => "882",
        "request" => "/docs/html/functions_rela.html",
      "timestamp" => "30/Apr/2017:04:30:13 +0000",
           "path" => "/root/access_log",
          "ident" => "-"
}



```

6. Check 



Let's go ahead and see what we got in our elastic search index now.


```
curl -H "Content-Type: application/json" -XGET 127.0.0.1:9200/_cat/indices?v
health status index                      uuid                   pri rep docs.count docs.deleted store.size pri.store.size
yellow open   movies                     HJxCHMVGRAiWF1SypP6BpQ   1   1          5            0     10.8kb         10.8kb
yellow open   shakespeare                ZfbMi35OSsqHzDPFRRlN6A   1   1     111396            0     19.5mb         19.5mb
yellow open   logstash-2019.08.26-000001 n962dAmfQWiYClke1TK7cw   1   1     102972            0     31.7mb         31.7mb
yellow open   shakespwa                  m-PNrrUaRmOS5-eVmIlUhA   1   1          0            0       283b           283b
```



```json

# curl -H "Content-Type: application/json" -XGET 127.0.0.1:9200/logstash-2019.08.26-000001/_search?pretty
{
  "took" : 87,
  "timed_out" : false,
  "_shards" : {
    "total" : 1,
    "successful" : 1,
    "skipped" : 0,
    "failed" : 0
  },
  "hits" : {
    "total" : {
      "value" : 10000,
      "relation" : "gte"
    },
    "max_score" : 1.0,
    "hits" : [
      {
        "_index" : "logstash-2019.08.26-000001",
        "_type" : "_doc",
        "_id" : "qXlzzWwBReKPgEIsNZMA",
        "_score" : 1.0,
        "_source" : {
          "@version" : "1",
          "httpversion" : "1.1",
          "referrer" : "\"-\"",
          "host" : "els1",
          "@timestamp" : "2017-04-30T04:28:11.000Z",
          "clientip" : "216.244.66.246",
          "auth" : "-",
          "response" : "200",
          "message" : "216.244.66.246 - - [30/Apr/2017:04:28:11 +0000] \"GET /docs/triton/pages.html HTTP/1.1\" 200 5639 \"-\" \"Mozilla/5.0 (compatible; DotBot/1.1; http://www.opensiteexplorer.org/dotbot, help@moz.com)\"",
          "verb" : "GET",
          "agent" : "\"Mozilla/5.0 (compatible; DotBot/1.1; http://www.opensiteexplorer.org/dotbot, help@moz.com)\"",
          "bytes" : "5639",
          "request" : "/docs/triton/pages.html",
          "timestamp" : "30/Apr/2017:04:28:11 +0000",
          "path" : "/root/access_log",
          "ident" : "-"
        }
      },
      {
        "_index" : "logstash-2019.08.26-000001",
        "_type" : "_doc",
        "_id" : "qnlzzWwBReKPgEIsNZMA",
        "_score" : 1.0,
        "_source" : {
          "@version" : "1",
          "httpversion" : "1.1",
          "referrer" : "\"-\"",
          "host" : "els1",
          "@timestamp" : "2017-04-30T04:29:44.000Z",
          "clientip" : "199.21.99.207",
          "auth" : "-",
          "response" : "200",
          "message" : "199.21.99.207 - - [30/Apr/2017:04:29:44 +0000] \"GET /docs/triton/class_triton_1_1_wind_fetch-members.html HTTP/1.1\" 200 1845 \"-\" \"Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)\"",
          "verb" : "GET",
          "agent" : "\"Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)\"",
          "bytes" : "1845",
          "request" : "/docs/triton/class_triton_1_1_wind_fetch-members.html",
          "timestamp" : "30/Apr/2017:04:29:44 +0000",
          "path" : "/root/access_log",
          "ident" : "-"
        }
      },


```

