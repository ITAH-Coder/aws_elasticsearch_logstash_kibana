# Analyzers


Mappiing is a schema definition. Elasticsearch has resonable defaults, but sometimes you need customize them.
So mappings can define field types like we talked about and other field types besides date can include
strings, bytes, short integers, integers, long integers, floating point numbers, double precision floating
point numbers, and boolean values as well.

 ![mapping](../images/els1.png)

So basically there are three things an analyser can do.
One is ***character filters***, so an analyzer can remove HTML encoding and do things like convert Ampersand
to the word "and" for example. The idea here is that if you apply the same analyzer to both your search query,
and the information that's indexed, you can get rid of all those discrepancies that might apply between
the two.


 ![analyzers1](../images/els2.png)


For analyzers themselves there are several choices.

 ![analyzers2](../images/els3.png)
 
 
 
 
Example: get film with genre: drama


```ruby

 curl -H "Content-Type: application/json" -H "Accept: application/json" -XGET 127.0.0.1:9200/movies/_search?pretty -d'
{
"query": {
"match_phrase": {
"genre": "drama"
}
}
}
'
```
