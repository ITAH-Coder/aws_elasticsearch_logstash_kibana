   
   **GET:**
   
   Desription of mapping:
   ```ruby
   curl -H "Content-Type: application/json" -H "Accept: application/json" -XGET 127.0.0.1:9200/movies/
   ```
   
   
   Get all movies (pretty output)
   
   ```ruby
   
curl -H "Content-Type: application/json" -H "Accept: application/json" -XGET 127.0.0.1:9200/movies/_search?pretty
```

Get accurate item:

```ruby
 curl -H "Content-Type: application/json" -XGET 127.0.0.1:9200/movies/_doc/58559?pretty
{
  "_index" : "movies",
  "_type" : "_doc",
  "_id" : "58559",
  "_version" : 1,
  "_seq_no" : 3,
  "_primary_term" : 1,
  "found" : true,
  "_source" : {
    "id" : "58559",
    "title" : "Dark Knight, The",
    "year" : 2008,
    "genre" : [
      "Action",
      "Crime",
      "Drama",
      "IMAX"
    ]
  }
}
```


UPADTE:

```ruby
curl -H "Content-Type: application/json" -XPOST 127.0.0.1:9200/movies/_doc/109487/_update -d
```

DELETE:

```ruby
url -H "Content-Type: application/json" -XDELETE 127.0.0.1:9200/movies/_doc/109487?pretty
```

