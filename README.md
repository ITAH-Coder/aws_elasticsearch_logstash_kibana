
<img src="images/elastic.png" alt="Your image title" float: right width="150"/> 

#

Elastcsearch is a real-time, distributed storage, search, and analytics engine. It can be used for many purposes, but one context where it excels is indexing streams of semi-structured data, such as logs or decoded network packets.

Various applications send log events to **Logstash**, which gathers the messages, converts them into json documents, and stores them in an **Elasticsearch cluster**. We can use **Kibana** as a front-end client to filter and display messages from the Elasticsearch cluster. Below are the core components of our ELK stack, and additional components used. Since we utilize more than the core ELK components, we'll refer to our stack as "ELK+"


This repo contains: 
- docs
- projects: 
  Bulid AWS Elasticsearch Cluster (composed - two nodes) - created using IaaC TERRAFORM 


# 
### Documentation


[![Docs](https://img.shields.io/badge/Analyzers&Tokenizer-Docs-red)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/analyzers_tokenizers.md)
[![Docs](https://img.shields.io/badge/Create&Import&Data-Docs-red)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/create_import_data.md)
[![Docs](https://img.shields.io/badge/UpdateData-Docs-red)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/update_data.md)
[![Docs](https://img.shields.io/badge/DeleteData-Docs-red)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/delete_data.md)
[![Docs](https://img.shields.io/badge/SearchData-Docs-green)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/search.md)
[![Docs](https://img.shields.io/badge/SortData-Docs-green)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/sort.md)
[![Docs](https://img.shields.io/badge/FiltersUpdateData-Docs-green)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/filters.md)
[![Docs](https://img.shields.io/badge/FiltersUpdateData-Docs-green)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/insert_delete_update_commands.md)
[![Docs](https://img.shields.io/badge/ImprtingDataWithScript-Docs-yellow)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/importing_data.md)
[![Docs](https://img.shields.io/badge/Aggregation-Docs-brown)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/aggregation.md)
[![Docs](https://img.shields.io/badge/logstash-Docs-blue)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/logstash.md)
[![Docs](https://img.shields.io/badge/logstash_install_configure-Docs-blue)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/logstash_install_configure.md)
[![Docs](https://img.shields.io/badge/Filebeat-Docs-blue)](https://github.com/ITAndreHoch/Elasticsearch/blob/master/Docs/filebeat.md)

# 
#### Demployment of Elasticsearch domains:
(two node's cluster in differnet AZ)

Parameters:

```
Elasticsearch version:   7.1
Availability zones:      2
Instance typem:          4.large.elasticsearch
Number of instances:     2
Storage type:            EBS
EBS volume type:         General Purpose (SSD)
EBS volume size:         10 GB
Encryption at rest:      Enabled
Node-to-node encryption: Disabled
Upgrade status:          Start hour for the daily automated snapshot23:00 UTC
```


There are a few steps to getting an Amazon Elasticsearch Service domain up and running:

- Define domain
- Configure cluster
- Check




- Define domain
- Configure cluster
- Check

Diagram:

<img src="images/elastic_diagram.png" alt="Your image title" float: right width="600"/>

Testing:

For the security group, specify two inbound rules:

```
Type	Protocol	Port Range	Source
SSH (22)	TCP (6)	22	your-cidr-block
HTTPS (443)	TCP (6)	443	your-security-group-id

```
The first rule lets you SSH into your EC2 instance. The second allows the EC2 instance to communicate with the Amazon ES domain over HTTPS.

From the terminal, run the following command:

```
ssh -i ~/.ssh/your-key.pem ec2-user@your-bastion-public-ip -N -L 9200:vpc-your-amazon-es-domain.region.es.amazonaws.com:443
```


This command creates an SSH tunnel that forwards requests to https://localhost:9200 to your Amazon ES domain through the EC2 instance. By default, Elasticsearch listens for traffic on port 9200. Specifying this port simulates a local Elasticsearch install, but use whichever port you'd like.

The command provides no feedback and runs indefinitely. To stop it, press Ctrl + C.

```
Navigate to https://localhost:9200/_plugin/kibana/ in your web browser. You might need to acknowledge a security exception.
```

Alternately, you can send requests to https://localhost:9200 using curl, Postman.


