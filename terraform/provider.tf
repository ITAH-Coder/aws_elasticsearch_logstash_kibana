provider "aws" {
  # No credentials explicitly set here because they come from either the  # environment or the global credentials file.
  region = "${var.aws_region}"
}
