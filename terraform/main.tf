#    Elasticsearch Cluster

# introduction of Local Values configuration language feature
terraform {
  required_version = ">= 0.10.3"
}

locals {
  tags = "${merge(map("Region", "${var.aws_region}"), map("Environment", "${var.environment}"), var.default_tags)}"
}

# --- ELK Cluster --------------------

### Security Group for ELK
resource "aws_security_group" "elk" {
  vpc_id = "${var.vpc_id}"

  # Inbound rule - inbound traffic is allowed only from VPC IP range
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.130.0.0/16"]
    description = "Allow incomming ssh trafic (for tunneling)"
  }
  ingress {
    from_port   = "${var.es_port}"
    to_port     = "${var.es_port}"
    protocol    = "tcp"
    cidr_blocks = ["10.130.0.0/16"]
    description = "Allow incomming traffic from admin VPC IP range"
  }
  # Outbound rule - allow connections in VPC IP range
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["${var.vpc_cidr}"]
    description = "Allow connections in VPC IP range"
  }
  tags = "${merge(map("Name", "sg-rds-${var.project_name}-${var.environment}"), map("Project", "${var.project_name}"), local.tags)}"
}
### END


resource "aws_iam_service_linked_role" "es" {
  aws_service_name = "es.amazonaws.com"
  description      = "AWSServiceRoleForAmazonElasticsearchService Service-Linked Role"
}

resource "aws_elasticsearch_domain" "elk_domain" {
  domain_name           = "${var.project_name}-in-${var.environment}"
  elasticsearch_version = "7.1"

  cluster_config {
    instance_type            = "${var.elk_instance_type}"
    instance_count           = "${var.elk_instance_count}"
    dedicated_master_enabled = "${var.dedicated_master_enabled}"
    zone_awareness_enabled   = "${var.zone_awareness_enabled}"
  }

  vpc_options {
    subnet_ids = ["${var.elk_subnet_ids[0]}", "${var.elk_subnet_ids[1]}"]
    security_group_ids = ["${aws_security_group.elk.id}"]
  }

  ebs_options {
    ebs_enabled = true
    volume_size = "${var.ebs_volume_size}"
    volume_type = "${var.ebs_volume_type}"
  }

  encrypt_at_rest {
    enabled    = "${var.encrypt_at_rest_enabled}"
    kms_key_id = "${var.encrypt_at_rest_kms_key_id}"
  }

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  tags = "${map(
    "Name", format("%s-in-%s-%s", var.project_name, var.aws_region, var.environment),
    "Domain", format("%s-in-%s-%s", var.project_name, var.aws_region, var.environment),
    "Region", var.aws_region,
    "NR_ELK", "1")
  }"

  depends_on = [
    "aws_iam_service_linked_role.es",
  ]
}


resource "aws_elasticsearch_domain_policy" "elk_main" {
  domain_name = "${aws_elasticsearch_domain.elk_domain.domain_name}"
  access_policies = <<POLICIES
{
    "Version": "2012-10-17",
    "Statement": [
      {
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "*"
        ]
      },
      "Action": [
        "es:*"
      ],
      "Resource": "${aws_elasticsearch_domain.elk_domain.arn}/*"
     }
   ]
}
POLICIES
}


###################
#  EC2-logstash
###################

module "elk_secret" {
  source     = "git::https://git-codecommit.eu-central-1.amazonaws.com/v1/repos/secretsmanager_secret.tfmodule"
  secret_arn = "${var.elk_secret_arn}"
}

module "ssh_admin_keypair" {
  source     = "git::https://git-codecommit.eu-central-1.amazonaws.com/v1/repos/secretsmanager_secret.tfmodule"
  secret_arn = "${var.ssh_keypair_arn}"
}

data "template_file" "authorized_keys" {
  template = "${file("templates/authorized_keys.sh.tpl")}"

  vars {
    ssh_keypair_id_rsa_pub = "${base64decode(lookup(module.ssh_admin_keypair.secret, "public"))}"
  }
}

data "aws_route53_zone" "aws_emagin_eu" {
  name         = "aws.emagin.eu"
  private_zone = false
}


module "logstash_server" {
  source = "git::https://git-codecommit.eu-central-1.amazonaws.com/v1/repos/ec2instance.tfmodule"

  vpc_id = "${var.vpc_id}"

  private_ips = ["${var.ec2_private_ips}"]
  subnet_ids  = ["${var.ec2_subnet_ids}"]
  hostname    = "${var.hostname}"
  azs         = "${var.azs}"
  project     = "${var.project_name}"
  environment = "${var.environment}"
  zone_id     = "${data.aws_route53_zone.aws_emagin_eu.zone_id}"


  create_route53_record = true
  assign_private_ip      = true
  custom_route53_records = ["logstash"]

  ami                   = "${var.ec2_ami}"
  instance_type         = "${var.ec2_instance_type}"
  root_volume_size      = "${var.ec2_root_volume_size}"

  # https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSVolumeTypes.html
  root_volume_type = "gp2"
  user_data        = "${data.template_file.authorized_keys.rendered}"

  volume_snapshots_active = "${var.environment_volume_snapshots_active}"

  # How often this lifecycle policy should be evaluated. 2,3,4,6,8,12 or 24 are valid values
  volume_snapshots_interval          = 24
  volume_snapshots_retain_rule_count = 7
  snapshots_execution_role_arn       = "${aws_iam_role.dlm_lifecycle_role.arn}"

  ingres_rules = "${var.ec2_ingres_rules}"
  egress_rules = "${var.ec2_egress_rules}"

  tags = "${merge(map("Project", "${var.project_name}"), map("AnsibleID", "${var.hostname}-server"), local.tags)}"
}
